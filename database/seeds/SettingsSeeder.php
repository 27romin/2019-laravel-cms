<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::truncate();
        // Website Name
        Setting::updateOrCreate(
            ['key' => 'website_name'],
            [
                'name' => 'Website Name',
                'key' => 'website_name',
                'value' => 'Your Website Name'
            ]
        );

        // Website URL
        Setting::updateOrCreate(
            ['key' => 'website_url'],
            [
                'name' => 'Website URL',
                'key' => 'website_url',
                'value' => 'http://www.yourwebsite.com'
            ]
        );
        
        // Media Types
        Setting::updateOrCreate(
            ['key' => 'media_types'],
            [
                'name' => 'Media Types',
                'key' => 'media_types',
                'value' => 'png, gif, jpg'
            ]
        );

        // Media Max Upload Size
        Setting::updateOrCreate(
            ['key' => 'media_max_size'],
            [
                'name' => 'Media Max Upload Size (MB)',
                'key' => 'media_max_size',
                'value' => '2'
            ]
        );
    }
}
