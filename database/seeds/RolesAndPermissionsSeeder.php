<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // Pages
        Permission::create(['name' => 'index pages']);
        Permission::create(['name' => 'create pages']);
        Permission::create(['name' => 'edit pages']);
        Permission::create(['name' => 'delete pages']);
        // Users
        Permission::create(['name' => 'index users']);
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'delete users']);
        // User Permission
        Permission::create(['name' => 'create user permissions']);
        Permission::create(['name' => 'edit user permissions']);
        // Files
        Permission::create(['name' => 'index files']);
        Permission::create(['name' => 'edit files']);
        // Media
        Permission::create(['name' => 'index media']);
        Permission::create(['name' => 'create media']);
        Permission::create(['name' => 'edit media']);
        Permission::create(['name' => 'delete media']);
        // Themes
        Permission::create(['name' => 'index themes']);
        Permission::create(['name' => 'create themes']);
        Permission::create(['name' => 'edit themes']);
        Permission::create(['name' => 'delete themes']);
        // Settings
        Permission::create(['name' => 'index settings']);
        Permission::create(['name' => 'edit settings']);
        
        // create roles and assign created permissions
        $admin = Role::create(['name' => 'admin']);
        $admin->givePermissionTo([
            'index pages',
            'create pages',
            'edit pages',
            'delete pages',
            // USERS
            'index users',
            'create users',
            'edit users',
            'delete users',
            // USER PERMISSIONS
            'create user permissions',
            'edit user permissions',
            // FILES
            'index files',
            'edit files',
            // MEDIA
            'index media',
            'create media',
            'edit media',
            'delete media',
            // THEMES
            'index themes',
            'create themes',
            'edit themes',
            'delete themes',
            // SETTINGS
            'index settings',
            'edit settings'
        ]);

        $moderator = Role::create(['name' => 'moderator']);
        $moderator->givePermissionTo([
            'index pages',
            'create pages',
            'edit pages',
            'delete pages',
            // MEDIA
            'index media',
            'create media',
            'edit media',
            'delete media',
            // THEMES
            'index themes',
            'create themes',
            'edit themes',
        ]);
    }
}
