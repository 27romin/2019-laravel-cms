<?php

use Illuminate\Database\Seeder;
use App\Page;
use App\Content;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = Page::updateOrCreate(
            ['title' => 'Homepage', 'homepage' => 1],
            [
                'title' => 'Homepage',
                'status' => 'published',
                'homepage' => 1,
            ]
        );
        
        Content::updateOrCreate(
            [
                'model_type' => 'App\Page',
                'model_id' => $page->id,
            ],
            [
                'model_type' => 'App\Page',
                'model_id' => $page->id,
            ]
        );
    }
}
