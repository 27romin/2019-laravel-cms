<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('passport:install', ['--force' => true]);

        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@website.com',
            'password' => bcrypt('password')
        ]);

        $user->assignRole('admin');
        $user->assignRole('moderator');
    }
}
