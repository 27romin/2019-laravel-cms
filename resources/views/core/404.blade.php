@extends( config('theme.directory') . '.layout')

@section('content')
  <div class="container text-center">
        <h2>404</h2>
        <p>Page not found.</p>
  </div>
@endsection