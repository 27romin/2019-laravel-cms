@extends( config('theme.directory') . '.layout')

@section('content')
  <div class="container">
      <h1>
            Core: {{ $content->title }}
      </h1>
      <p>
          <small class="text-muted">Posted: {{ $content->created_at }}</small>
      </p>
      <p>
            {!! $content->body !!}
      </p>
  </div>
@endsection