<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <link href="{{ asset('css/admin.css') }}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('vendor/slick/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/slick/slick-theme.css') }}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.18.7/slimselect.min.css" rel="stylesheet"></link>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
        <script type="text/javascript">
            var laravel = @json([
                'url' => env('APP_URL'),
                'csrf' => csrf_token(),
                'auth' => auth()->user()
            ]);
        </script>

        
    </head>
<body>

    <div id="app">
        <!-- Navigation -->
        <nav class="navigation">
            <ul class="navigation__menu">
                <li class="navigation__item">
                    <router-link :to="{ name: 'admin.home.index' }">
                        <i class="fas fa-home"></i>
                        <span>Home</span>
                    </router-link>
                </li>

                @can(['index pages'])
                    <li class="navigation__item">
                        <router-link :to="{ name: 'admin.pages.index' }">
                            <i class="fas fa-file-alt"></i>
                            <span>Pages</span>
                        </router-link>
                    </li>
                @endcan

                @can(['index users'])
                    <li class="navigation__item">
                        <router-link :to="{ name: 'admin.users.index' }">
                            <i class="fas fa-users"></i>
                            <span>Users</span>
                        </router-link>
                    </li>
                @endcan

                @can(['index files'])
                    <li class="navigation__item">
                        <router-link :to="{ name: 'admin.files.index' }">
                            <i class="fas fa-code"></i>
                            <span>Files</span>
                        </router-link>
                    </li>
                @endcan

                @can(['index media'])
                    <li class="navigation__item">
                        <router-link :to="{ name: 'admin.media.index' }">
                            <i class="fas fa-images"></i>
                            <span>Media</span>
                        </router-link>
                    </li>
                @endcan

                @can(['index themes'])
                    <li class="navigation__item">
                        <router-link :to="{ name: 'admin.themes.index' }">
                            <i class="fas fa-fill-drip"></i>
                            <span>Themes</span>
                        </router-link>
                    </li>
                @endcan

                @can(['index settings'])
                    <li class="navigation__item">
                        <router-link :to="{ name: 'admin.settings.index' }">
                            <i class="fas fa-cogs"></i>
                            <span>Settings</span>
                        </router-link>
                    </li>
                @endcan

            </ul>
        </nav>

        <app></app>
    </div>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.1/ace.js" charset="utf-8"></script>
    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/slick/slick.js') }}"></script>
</body>
</html>