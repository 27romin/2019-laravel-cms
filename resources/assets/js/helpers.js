/*
    Helpers.js
*/
var helper = {

    /**
     * Trim Words.
     * 
     * @param {string} string 
     * @param {integer} length 
     */
    trimWords(string, length) 
    {
        return string.length > length ? 
        string.substring(0, length - 3) + '...' : string
    },

    /**
     * Capitalize words.
     * 
     * @param {string} string 
     */
    capitalize(string) 
    {
        return string.replace(/\w\S*/g, function(text) {
            return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
        });
    },

    /**
     * Remove space and replace with hyphen or underscore.
     * 
     * @param attribute 
     * @param {string} value 
     */
    alphaDash(attribute, value) 
    {
        return attribute.replace(/\s+/g, value)
    },

    /**
     * Flatten by object property.
     * 
     * @param {object} object 
     * @param {string} prop 
     */
    flatten(object, prop) {
        return object.map(function (object) {
            return object && object[prop]
        });
    },

    /* 
    * Returns TRUE if the first array contains all elements
    * from the second array.
    *
    * @param {array} superset
    * @param {array} subset
    *
    * @returns {boolean}
    */
   inArray (superset, subset) {
        return subset.every(function (value) {
            return superset && (superset.indexOf(value) >= 0);
        });
   },

   /**
    * Removed duplicate values from an array.
    * 
    * @param {*} array 
    */
    unique(array) {
        return array.filter((elem, pos, arr) => {
            return arr.indexOf(elem) == pos;
        });
    },

    /**
     * Merges all object based errors into an unique 
     * array.
     * 
     * @param {*} array 
     */
    errors(array) {
        let arr = []
        
        for (var key in array) {
            if (array.hasOwnProperty(key)) {
              arr.push((array[key][0]))
            }
        }
        return this.unique(arr)
    },

    /**
     * Based on a vowl, prefix word. Eg an Admin or 
     * a Moderator
     * 
     * @param {*} prefix 
     * @param {*} word 
     */
    isVowel(nv_prefix, v_prefix, word) {
        let contain = ['a', 'e', 'i', 'o', 'u'].indexOf(word[0].toLowerCase()) !== -1;
        let prefix = (contain) ? v_prefix  : nv_prefix;
        return  prefix + ' ' + word;
    },

    /**
     * 
     */
    random() {
        return '_' + Math.random().toString(36).substr(2, 9);
    }
}

export default helper
