/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueMoment from 'vue-moment'
import VueCodemirror from 'vue-codemirror'
import _ from 'lodash'

Vue.use(VueCodemirror)
Vue.use(VueRouter)
Vue.use(VueResource);
Vue.use(VueMoment);

/**
 * Global Components.
 */
Vue.component('Pagination', require('./components/utilities/Pagination.vue'));
Vue.component('Upload', require('./components/utilities/Upload.vue'));
Vue.component('UploadProgress', require('./components/utilities/UploadProgress.vue'));

import App from '@js/components/App'

// HOME
import HomeIndex from '@js/components/home/Index'

// PAGES
import PageIndex from '@js/components/pages/Index'
import PageCreate from '@js/components/pages/Create'
import PageEdit from '@js/components/pages/Edit'

// USERS
import UserIndex from '@js/components/users/Index'
import UserCreate from '@js/components/users/Create'
import UserEdit from '@js/components/users/Edit'

// FILES
import FileIndex from '@js/components/files/Index'

// MEDIA
import MediaIndex from '@js/components/media/Index'

// SETTINGS
import SettingsIndex from '@js/components/settings/Index'

// THEMES
import ThemesIndex from '@js/components/themes/Index'
import ThemesCreate from '@js/components/themes/Create'
import ThemesEdit from '@js/components/themes/Edit'

import auth from '@js/auth'

const router = new VueRouter({
    mode: 'history',
    routes: [
        // Home
        {
            path: '/admin',
            name: 'admin.home.index',
            component: HomeIndex
        },
        // Pages
        {
            path: '/admin/pages',
            name: 'admin.pages.index',
            component: PageIndex,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['index pages']) ? next() : next(false)
                })
            }
        },
        {
            path: '/admin/pages/create',
            name: 'admin.pages.create',
            component: PageCreate,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['create pages']) ? next() : next(false)
                })
            }
        },
        {
            path: '/admin/pages/:id/edit',
            name: 'admin.pages.edit',
            component: PageEdit,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['edit pages']) ? next() : next(false)
                })
            }
        },
        // Users
        {
            path: '/admin/users',
            name: 'admin.users.index',
            component: UserIndex,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['index users']) ? next() : next(false)
                })
            }
        },
        {
            path: '/admin/users/create',
            name: 'admin.users.create',
            component: UserCreate,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['create users']) ? next() : next(false)
                })
            }
        },
        {
            path: '/admin/users/:id/edit',
            name: 'admin.users.edit',
            component: UserEdit,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['edit users']) ? next() : next(false)
                })
            }
        },
        // Files
        {
            path: '/admin/files',
            name: 'admin.files.index',
            component: FileIndex,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['index files']) ? next() : next(false)
                })
            }
        },
        // Media
        {
            path: '/admin/media',
            name: 'admin.media.index',
            component: MediaIndex,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['index media']) ? next() : next(false)
                })
            }
        },
        // Settings
        {
            path: '/admin/settings',
            name: 'admin.settings.index',
            component: SettingsIndex,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['index settings']) ? next() : next(false)
                })
            }
        },
        // Themes
        {
            path: '/admin/themes',
            name: 'admin.themes.index',
            component: ThemesIndex,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['index themes']) ? next() : next(false)
                })
            }
        },
        {
            path: '/admin/themes/create',
            name: 'admin.themes.create',
            component: ThemesCreate,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['create themes']) ? next() : next(false)
                })
            }
        },
        {
            path: '/admin/themes/:id/edit',
            name: 'admin.themes.edit',
            component: ThemesEdit,
            beforeEnter: (to, from, next) => {
                auth.user().then(user => {
                    auth.hasPermissions(user, ['edit themes']) ? next() : next(false)
                })
            }
        }

    ],
});



const app = new Vue({
    el: '#app',
    components: { App },
    router,
});


