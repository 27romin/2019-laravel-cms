import axios from 'axios'
import helper from '../js/helpers';
var auth = {

    /**
     * Fetch user endpoint.
     */
    user() {
        return axios.get('/api/user');
    },

    /**
     * Set Permissions.
     * 
     * @param {array} user 
     */
    setPermissions(user)
    {
        let permissions = user.roles.flatMap(roles => roles.permissions)
        return _.uniqBy(permissions, 'name')
    },

    /**
     * Check is user has permissions.
     * 
     * @param {array} user 
     * @param {array} permissions 
     */
    hasPermissions(user, permissions) {
        let userPermissions = helper.flatten(this.setPermissions(user.data), 'name')
        return helper.inArray(userPermissions, permissions)
    }
}

export default auth

