<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    protected $guarded = ['id'];

    /**
     * Using checkboxes in Vue JS returns a string / null 
     * value. Therefore, a mutator is required to format 
     * the returning value into an integer.
     *
     * @param  string  $value
     * @return void
     */
    public function setActiveAttribute($value)
    {
        $active = $value ? 1 : 0;
        $this->attributes['active'] = $active;
    }
}
