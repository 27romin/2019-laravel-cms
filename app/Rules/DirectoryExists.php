<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use File;

class DirectoryExists implements Rule
{
    protected $directory;
    protected $subDirectory;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($directory, $subDirectory)
    {
        $this->directory = $directory;
        $this->subDirectory = $subDirectory;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $name)
    {
        $directory = $this->directory;
        $subDirectory = $this->subDirectory;

        if(File::isDirectory( $directory( $subDirectory . strtolower(str_slug($name, '_')) ))) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'There is aleady a theme with that name.';
    }
}
