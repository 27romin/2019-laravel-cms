<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Media;
use File;

class MediaExists implements Rule
{
    protected $id;
    protected $type;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id, $type)
    {
        $this->id = $id;
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $name)
    {
        $media = Media::find($this->id);
        $path = config('storage.media') . $name . '.' . $this->type;

        if(File::exists($path) && $media->name != $name) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The media file already exists';
    }
}
