<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\Resource;
use App\Page;
use App\Theme;
use App\Setting;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    /**
        * Perform post-registration booting of services.
        *
        * @return void
        */
    public function boot()
    {
        Resource::withoutWrapping();

        if (! app()->runningInConsole()) {
            $appName = Setting::where('key', 'website_name')->first();
            $appUrl = Setting::where('key', 'website_url')->first();
            $theme = Theme::where('active', true)->first();
            $homepage = Page::where('homepage', true)
            ->withoutTrashed()
            ->latest()
            ->first();
            
            config([
                'app.home' => ($homepage) ? (string) $homepage->content->slug : '/',
                'app.name' => $appName->value,
                'app.url' => $appUrl->value,
                'theme.directory' => $theme->theme_directory ?? 'core',
            ]);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
