<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $guarded = ['id'];

    /**
     * Set table to media.
     *
     * @var string
     */
    protected $table = 'media';
}
