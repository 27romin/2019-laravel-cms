<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $guarded = ['id'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Model points towards the child relationship, this
     * can be any related model. For example, page or
     * post.
     *
     * @return void
     */
    public function model()
    {
        return $this->morphTo();
    }
}
