<?php

namespace App\Http\Controllers;

class AdminController extends Controller
{
    /**
     * Render admin template.
     *
     * @return void
     */
    public function index()
    {
        return view('admin.template');
    }
}
