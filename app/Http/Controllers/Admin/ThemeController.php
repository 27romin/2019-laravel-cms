<?php

namespace App\Http\Controllers\Admin;

use File;
use Storage;
use App\Theme;
use Illuminate\Http\Request;
use App\Rules\DirectoryExists;
use App\Http\Controllers\Controller;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Validator;

class ThemeController extends Controller
{
    /**
     * Fetch all themes.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $themes = Theme::latest()->get();
        return response()->json($themes, 202);
    }

    /**
     * Show theme.
     *
     * @param \App\Theme $theme
     * @return \Illuminate\Http\Response
     */
    public function show(Theme $theme)
    {
        return response()->json($theme, 202);
    }

    /**
     * Validate and store theme.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'media' => 'required',
            'theme_directory' => ['required', new DirectoryExists('resource_path', 'views/')],
        ]);
        
        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        if ($request->activate) {
            $active = Theme::where('active', true)->first();
            optional($active)->update(['active' => false]);
        }

        $file = new Filesystem();
        $image = $request->file('media')->store('themes', 'public');

        $theme = Theme::create([
            'image' => $file->name($image) . '.' . $file->extension($image),
            'name' => $request->name,
            'active' => $request->activate,
            'path' => $file->dirname($image),
            'theme_directory' => $request->theme_directory,
            'description' => $request->description,
        ]);

        File::copyDirectory(resource_path('views/core'), resource_path('views/' . strtolower(str_slug($request->theme_directory, '_'))));
        
        return response()->json($theme, 202);
    }

    /**
     * Validate and update theme.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\THeme $theme
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Theme $theme)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        if ($request->activate) {
            $active = Theme::where('active', true)->where('id', '!=', $theme->id)->first();
            optional($active)->update(['active' => false]);
        }

        if ($request->media) {
            Storage::disk('public')->delete($theme->path . '/' .$theme->image);
            $image = $request->file('media')->store('themes', 'public');
        }

        $file = new Filesystem();

        $theme->update([
            'image' => $request->media ? $file->name($image) . '.' . $file->extension($image) : $theme->image,
            'name' => $request->name,
            'active' => $request->activate,
            'path' => $request->media ? $file->dirname($image) : $theme->path,
            'description' => $request->description,
        ]);
    }

    /**
     * Delete theme.
     *
     * @param \App\Theme $theme
     * @return \Illuminate\Http\Response
     */
    public function destroy(Theme $theme)
    {
        Storage::disk('public')->delete($theme->path . '/' .$theme->image);
        File::deleteDirectory(resource_path('views/' . $theme->theme_directory));
        $theme->delete();
    }

    /**
     * Activate / Deactivate theme.
     *
     * @param \App\Theme $theme
     * @return \Illuminate\Http\Response
     */
    public function switch(Theme $theme)
    {
        if (! $theme->active == true) {
            $current = Theme::where('active', true)->first();
            optional($current)->update(['active' => false]);
        }
            
        $theme->update([
            'active' => ! $theme->active
        ]);
    }
}
