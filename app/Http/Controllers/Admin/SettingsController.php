<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    /**
     * Fetch all settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::get();
        return response()->json($settings, 200);
    }

    /**
     * Validate and update settings.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'settings.website_url.value' => 'required|active_url',
                'settings.media_max_size.value' => 'required|integer|between:1,10',
                'settings.*.value' => 'required'
            ],
            [
                'required' => 'All settings are required.',
                'active_url' => 'The website URL is not a valid.'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        foreach ($request->settings as $setting) {
            Setting::where('id', $setting['id'])->update([
                'value' => $setting['value']
            ]);
        }
    }
}
