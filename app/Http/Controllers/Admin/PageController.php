<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PageCollection;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    /**
     * Fetch all pages or filter by search.
     *
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\PageCollection
     */
    public function index(Request $request)
    {
        if ($request->filter != 'deleted') {
            $query = Page::search($request->search);
        } else {
            $query = Page::search($request->search)->onlyTrashed();
        }

        return (new PageCollection(
            $query->with('content')->latest()->paginate(10)
        ))->additional(['meta' => [
            'all' => Page::count(),
            'deleted' => Page::onlyTrashed()->count(),
        ]]);
    }

    /**
     * Show page.
     *
     * @param \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return response()->json($page->load('content'), 202);
    }

    /**
     * Validate and store page.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
            'title' => 'required',
            'status' => 'required',
            'content.slug' => 'nullable|max:50|unique:contents,slug',
            'template' => 'nullable|max:50|unique:pages,template,NULL,NULL,deleted_at,NULL'
        ],
        [ 'content.slug.unique' => 'The slug has already been taken.' ]
        );

        // Dot notation fix.
        $errors = [];
        foreach ($validator->errors()->messages() as $key => $value) {
            array_set($errors, $key, $value);
        }

        if ($validator->fails()) {
            return response()->json($errors, 422);
        }

        if ($request->homepage) {
            $homepages = Page::where('homepage', true)->get();
            $homepages->each(function ($homepage) {
                $homepage->update(['homepage' => false]);
            });
        }

        $page = Page::create([
            'title' => $request->title,
            'body' => $request->body,
            'status' => $request->status,
            'homepage' => $request->homepage,
            'template' => $request->template
        ]);

        $content = Content::create([
            'model_id' => $page->id,
            'model_type' => 'App\Page',
            'slug' => $request->content['slug'],
        ]);

        return response()->json($page->load('content'), 202);
    }

    /**
     * Validate and update page.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $validator = Validator::make(
            $request->all(),
            [
            'title' => 'required',
            'status' => 'required',
            'content.slug' => 'nullable|max:50|unique:contents,slug,' . $page->content->id,
            'template' => 'nullable|max:50|unique:pages,template,' . $page->id . ',id,deleted_at,NULL',
        ],
        [ 'content.slug.unique' => 'The slug has already been taken.' ]
        );

        // Dot notation fix.
        $errors = [];
        foreach ($validator->errors()->messages() as $key => $value) {
            array_set($errors, $key, $value);
        }

        if ($validator->fails()) {
            return response()->json($errors, 422);
        }

        if ($request->homepage) {
            $homepages = Page::where('homepage', true)
            ->where('id', '!=', $page->id)
            ->get();

            $homepages->each(function ($homepage) {
                $homepage->update(['homepage' => false]);
            });
        }

        $page->update([
            'title' => $request->title,
            'body' => $request->body,
            'status' => $request->status,
            'homepage' => $request->homepage,
            'template' => $request->template
        ]);

        $page->content->update($request->content);

        return response()->json($page, 202);
    }

    /**
     * Restore an array of soft deleted pages.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
        return Page::whereIn('id', $request->ids)->restore();
    }

    /**
     * Delete an array of pages.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $pages = Page::whereIn('id', $request->ids)
        ->withTrashed()
        ->get();

        $pages->each(function ($page) {
            if (! $page->trashed()) {
                $page->delete();
            } else {
                $page->forcedelete();
            }
        });
    }
}
