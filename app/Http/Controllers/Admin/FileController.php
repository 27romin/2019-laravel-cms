<?php

namespace App\Http\Controllers\Admin;

use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    /**
     * List all files from the given path.
     *
     * @param \Illuminate\Http\Request $request
     * @return Array $files
     */
    public function index(Request $request)
    {
        $files = [];
        $directory = File::allFiles(base_path() . $request->path);

        foreach ($directory as $file) {
            $files[] = (string) $file;
        }

        return $files;
    }

    /**
     * Get the file path and contents.
     *
     * @param \Illuminate\Http\Request $request
     * @return Array
     */
    public function show(Request $request)
    {
        return [
            'path' => $request->path,
            'contents' => File::get($request->path),
        ];
    }

    /**
     * Update file contents.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return file_put_contents($request->path, $request->contents);
    }
}
