<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserCollection;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Fetch all users or filter by search.
     *
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\UserCollection
     */
    public function index(Request $request)
    {
        if ($request->filter != 'deleted') {
            $query = User::search($request->search);
        } else {
            $query = User::search($request->search)->onlyTrashed();
        }

        return (new UserCollection(
            $query->latest()->paginate(10)
        ))->additional(['meta' => [
            'all' => User::count(),
            'deleted' => User::onlyTrashed()->count(),
        ]]);
    }

    /**
     * Show user.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return response()->json($user, 202);
    }

    /**
     * Validate and store user.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:users',
            'password' => 'min:6',
            'password_confirmation' => 'same:password'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $roles = collect($request->roles)->map(function ($role) {
            return collect($role)->only('name');
        })->flatten();

        $user->syncRoles($roles);

        return response()->json($user, 202);
    }

    /**
     * Validate and update user.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:25',
            'email' => 'required|email|unique:users,id,' . $user->id,
            'password' => ($request->password) ? 'min:6' : '',
            'password_confirmation' => 'same:password'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        $password = '';
        $hasPassword = $request->password && $request->password_confirmation;

        if ($hasPassword) {
            $password = Hash::make($request->password);
        }

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $hasPassword ? $password : $user->password
        ]);

        $roles = collect($request->roles)->map(function ($role) {
            return collect($role)->only('name');
        })->flatten();

        $user->syncRoles($roles);

        return response()->json($user, 202);
    }

    /**
     * Restore an array of soft deleted users.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
        return User::whereIn('id', $request->ids)->restore();
    }

    /**
     * Delete an array of users.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $users = User::whereIn('id', $request->ids)
        ->withTrashed()
        ->get();

        $users->each(function ($user) {
            if (! $user->trashed()) {
                $user->delete();
            } else {
                $user->forcedelete();
            }
        });
    }
}
