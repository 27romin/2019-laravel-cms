<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Fetch all roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles =  Role::get();
        return $roles->map(function ($role) {
            return $role->only(['id', 'name']);
        });
    }

    /**
     * Store role.
     *
     * @param \App\Role $role
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Role $role, User $user)
    {
        $user->assignRole($role->name);
        app()['cache']->forget('spatie.permission.cache');
    }

    /**
     * Delete role.
     *
     * @param \App\Role $role
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role, User $user)
    {
        $user->removeRole($role->name);
        app()['cache']->forget('spatie.permission.cache');
    }
}
