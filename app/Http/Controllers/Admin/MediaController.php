<?php

namespace App\Http\Controllers\Admin;

use File;
use Storage;
use App\Media;
use App\Rules\MediaExists;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Validator;

class MediaController extends Controller
{
    /**
     * Fetch all media.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $media =  Media::latest()->paginate(30);
        return response()->json($media, 200);
    }

    /**
     * Validate and store media.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'media' => 'max:30000'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        $file = new Filesystem();
        $upload = $request->file('media')->store('media', 'public');

        $media = Media::create([
            'size' => $file->size($request->file('media')),
            'name' => $file->name($upload),
            'path' => $file->dirname($upload),
            'type' => $file->extension($upload),
        ]);

        return response()->json($media, 202);
    }

    /**
     * Validate and update media.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'max:50', new MediaExists($request->id, $request->type)],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        $media = Media::find($request->id);
        $existing = config('storage.media') . $request->name . '.' . $request->type;

        if (! File::exists($existing)) {
            Storage::disk('public')->move(
                $media->path . '/' . $media->name . '.' . $media->type,
                $media->path . '/' . $request->name . '.' . $request->type
            );

            $media->update(['name' => $request->name]);
        }
        
        return response()->json($media, 200);
    }

    /**
     * Delete an array of media.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        foreach ($request->ids as $id) {
            $media = Media::find($id);
            if ($media) {
                Storage::disk('public')->delete($media->path . '/' . $media->name . '.'. $media->type);
                $media->delete();
            }
        }
    }
}
