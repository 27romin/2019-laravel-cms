<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Permission;
use App\Http\Controllers\Controller;

class PermissionsController extends Controller
{
    /**
     * Fetch all permissions.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::get();

        return $permissions->map(function ($permission) {
            return $permission->only(['id', 'name']);
        });
    }

    /**
     * Store permission.
     *
     * @param \App\Permission $permission
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Permission $permission, User $user)
    {
        $user->givePermissionTo($permission->name);
        app()['cache']->forget('spatie.permission.cache');
    }

    /**
     * Delete permission.
     *
     * @param \App\Permission $permission
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission, User $user)
    {
        $user->revokePermissionTo($permission->name);
        app()['cache']->forget('spatie.permission.cache');
    }
}
