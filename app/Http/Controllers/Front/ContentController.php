<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Content;
use App\Page;

class ContentController extends Controller
{
    /**
     * Load content and related polymorphic model.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function show($slug = null)
    {
        $content = Content::where('slug', $slug)->first();
        if ($content) {
            $content = Page::where('id', $content->model->id)->published()->first();
        }
        
        if (request()->is('/')) {
            $content = Page::where('homepage', true)->published()->latest()->first();
        }
   
        if (! $content) {
            return view(config('theme.directory') . '.404');
        }

        $template = 'pages.' . optional($content)->template;
        if (! optional($content)->template) {
            $template = 'pages.default';
        }
        
        return view(config('theme.directory') . '.' . $template, compact('content'));
    }

    /**
     * If a slug is not found, then search content.
     * Otherwise load a custom 404 page.
     *
     * @return \Illuminate\Http\Response
     */
    public function fallback($slug)
    {
        $content = Content::where('slug', $slug)->first();
        if (! $content) {
            return view(config('theme.directory') . '.404');
        }

        return $this->show($content->slug);
    }
}
