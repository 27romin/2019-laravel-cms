<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function boot()
    {
        parent::boot();
        
        static::deleting(function ($page) {
            if ($page->trashed()) {
                return $page->content->forcedelete();
            }
        });
    }
    
    /**
     * Return published pages.
     *
     * @param $query
     * @return void
     */
    public function scopePublished($query)
    {
        return $query->where('status', 'published');
    }
    
    /**
    * Content is the parent of a model, which contains
    * dependant information. For example, slug,
    * model type and model id.
    *
    * @return void
    */
    public function content()
    {
        return $this->morphOne(Content::class, 'model');
    }
    
    /**
    * Search by title scope.
    *
    * @param $query
    * @param string $search
    * @return void
    */
    public function scopeSearch($query, $search)
    {
        return $query->where('title', 'like', '%' . $search . '%');
    }
    
    /**
    * Using checkboxes in Vue JS returns a string / null
    * value. Therefore, a mutator is required to format
    * the returning value into an integer.
    *
    * @param  string  $value
    * @return void
    */
    public function setHomepageAttribute($value)
    {
        $homepage = $value ? 1 : 0;
        $this->attributes['homepage'] = $homepage;
    }
}
