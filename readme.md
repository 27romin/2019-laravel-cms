# Laravel CMS

Content management system with a media uploader, file manager, code editor, user roles, pages, changeable themes and settings.

## Models

```
/app
```

## Controllers

```
/app/Http/Controllers
```

## Views

```
/views
```

## Vue JS Components

```
/resources/assets/js/components
```

## Database and Seeders

```
/database
```
