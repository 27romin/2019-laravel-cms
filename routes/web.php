<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use App\Page;
// use File;
// Route::get('/testcode', function () {
    //some code
    // dd(File::exists(resource_path('views/' . config('theme.directory') . '/pages/') . 'test.blade.php'));
// });


// Route::get('/testfile', function() {
//     function parse_paths_of_files($array)
//     {
//         $result = array();
     
//         foreach ($array as $item)
//         {
//             $parts = explode('/', $item);
//             $current = &$result;
//             for ($i = 1, $max = count($parts); $i < $max; $i++) {
//                 if (!isset($current[$parts[$i-1]])) {
//                      $current[$parts[$i-1]] = array();
//                 }
//                 $current = &$current[$parts[$i-1]];
//             }
//             $current[] = $parts[$i-1];
//         }
     
//         return $result;
//     }
     
//     $test = array(
//         "dir1/dir2/dir3/file.ext",
//         "dir1/dir2/dir3/file2.ext",
//         "dir1/dir2/dir3/file3.ext",
//         "dir2/dir4/dir5/file.ext"
//     );
     
//     dd(parse_paths_of_files($test));
// });

Route::get('/home', function () {
    if (Auth::check()) {
        return redirect(config('app.home'));
    }
    return redirect('login');
});

Route::get('/admin/{any?}', 'AdminController@index')->where('any', '.*')->middleware('admin')->name('admin');
Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');

// use Cz\Git\GitRepository;
// use App\User;
// Route::get('/test', function () {
    // $repo = GitRepository::cloneRepository('https://github.com/czproject/git-php.git');
    // dd($repo);

    // $composer = shell_exec('composer remove facebook/graph-sdk --working-dir=/Applications/MAMP/htdocs/pagebuilder');
    // dd($composer);

    // $composer = shell_exec('composer require barryvdh/laravel-debugbar --dev --working-dir=/Applications/MAMP/htdocs/pagebuilder');
    // $artisan = \Artisan::call('vendor:publish', ['--provider' => 'Barryvdh\Debugbar\ServiceProvider']);
    // dump($composer);
    // dd($artisan);
// });


// use Illuminate\Http\Request;

// // First route that user visits on consumer app
// Route::get('/test', function () {
//     // Build the query parameter string to pass auth information to our request
//     $query = http_build_query([
//         'client_id' => 1,
//         'redirect_uri' => 'http://pagebuilder.test/callback',
//         'response_type' => 'code',
//         'scope' => 'place-orders'
//     ]);

//     // Redirect the user to the OAuth authorization page
//     return redirect('http://pagebuilder.test/oauth/authorize?' . $query);
// });

// // Route that user is forwarded back to after approving on server
// Route::get('/callback', function (Request $request) {
//     $http = new GuzzleHttp\Client;

//     $response = $http->post('http://pagebuilder.test/oauth/token', [
//         'form_params' => [
//             'grant_type' => 'authorization_code',
//             'client_id' => 2, // from admin panel above
//             'client_secret' => '2DuMfZ7GIjlRJonny70angfIgblFIHYa5dIcyNnx', // from admin panel above
//             'redirect_uri' => 'http://pagebuilder.test/callback',
//             'code' => $request->code // Get code from the callback
//         ]
//     ]);
//     // echo the access token; normally we would save this in the DB
//     return json_decode((string) $response->getBody(), true)['access_token'];
// });


// New way to handle 404

Route::namespace('Front')->group(function () {
    // CONTENT
    Route::get('{slug?}', 'ContentController@show');
    Route::fallback('ContentController@fallback');
});
