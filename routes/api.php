<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Admin')->prefix('admin')->middleware(['auth:api'])->group(function () {
    // PAGES
    Route::patch('pages/restore', 'PageController@restore');
    Route::resource('pages', 'PageController', ['as' => 'admin'])->except('destroy');
    Route::delete('pages', 'PageController@destroy');

    // USERS
    Route::patch('users/restore', 'UserController@restore');
    Route::resource('users', 'UserController', ['as' => 'admin'])->except('destroy');
    Route::delete('users', 'UserController@destroy');

    // ROLES
    Route::get('roles', 'RoleController@index');
    Route::post('roles/{role}/user/{user}', 'RoleController@store');
    Route::delete('roles/{role}/user/{user}', 'RoleController@destroy');

    // FILES
    Route::get('files', 'FileController@index');
    Route::patch('files', 'FileController@update');
    Route::get('files/edit', 'FileController@show');

    // MEDIA
    Route::resource('media', 'MediaController', ['as' => 'admin'])->except('destroy', 'update');
    Route::patch('media', 'MediaController@update');
    Route::delete('media', 'MediaController@destroy');

    // THEMES
    Route::resource('themes', 'ThemeController', ['as' => 'admin']);
    Route::patch('themes/{theme}/switch', 'ThemeController@switch');
    
    // SETTINGS
    Route::resource('settings', 'SettingsController', ['as' => 'admin'])->except('destroy', 'update');
    Route::patch('settings', 'SettingsController@update');
});
